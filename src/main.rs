use std::fs::File;
use std::io::{self, BufRead, Write};
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use std::sync::atomic::{AtomicBool, Ordering};

use clap::Parser;
use indicatif::{ProgressBar, ProgressStyle};

#[derive(Debug)]
struct Node {
    parent: String,
    fieldname: String,
    min: String,
    max: String,
    fieldtype: String,
    length: String,
}

/// program to map auto-gen descriptions into table-like mapping-guides
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// plain-text description file
    #[arg(short, long)]
    input: String,
    /// desired name of output file
    #[arg(short, long, default_value_t = String::from("output.csv"))]
    output: String,
}

fn main() -> io::Result<()> {
    let args = Args::parse();

    // Read input file
    let file_path = args.input.as_str();
    let reader = io::BufReader::new(File::open(file_path)?);

    // Set up output file
    let output_path = Path::new(&args.output);
    let mut output_file = File::create(output_path)?;

    // CSV header
    writeln!(output_file, "node name of parent;fieldname;min;max;type;length")?;

    let mut info_complete: bool = false;

    let mut current_node: Node = Node {
        parent: String::new(),
        fieldname: String::new(),
        min: String::new(),
        max: String::new(),
        fieldtype: String::new(),
        length: String::new(),
    };

    // Atomic flag to signal spinner thread to stop
    let should_stop = Arc::new(AtomicBool::new(false));

    // Create a progress bar wrapped in Arc<Mutex>
    let progress = Arc::new(Mutex::new(ProgressBar::new_spinner()));
    progress.lock().unwrap().set_style(ProgressStyle::default_spinner());

    let cloned_progress = progress.clone();
    let cloned_should_stop = should_stop.clone();

    let spinner_handle = thread::spawn(move || {
        while !cloned_should_stop.load(Ordering::Relaxed) {
            thread::sleep(Duration::from_millis(100));
            cloned_progress.lock().unwrap().tick();
        }
    });

    for line in reader.lines() {
        let line = line?;

        // each line that describes a path from the XML
        if line.trim().starts_with('/') {
            let mut parts: Vec<&str> = line.trim().split('/').collect();

            // clean up the parts
            parts = parts[1..].to_vec();

            // remove empty element from the end of the vector
            if parts[parts.len() - 1].is_empty() {
                parts.pop();
            }

	    if parts.len() == 1 {
		current_node.parent = String::from("root");
		current_node.fieldname = parts.pop().unwrap().to_owned();
	    } else {
		current_node.fieldname = parts.pop().unwrap().to_owned();
		current_node.parent = parts.pop().unwrap().to_owned();
	    }

        // each line containing metadata like min/max/datatype/length
        } else {
            current_node.fieldtype = if line.contains("String") {
                String::from("String")
            } else if line.contains("BigDecimal") {
                String::from("BigDecimal")
            } else if line.contains("Integer") {
                String::from("Integer")
            } else if line.contains("TimeStamp") {
                String::from("TimeStamp")
            } else {
                String::new()
            };

            let parts: Vec<&str> = line.split(",").collect();
            let min_part = parts
                .iter()
                .find(|&&part| part.trim().starts_with("Minimum"));
            let max_part = parts
                .iter()
                .find(|&&part| part.trim().starts_with("Maximum"));
            let length_part = parts
                .iter()
                .find(|&&part| part.trim().starts_with("Length"));

            if let Some(min_part) = min_part {
                if let Some(min_string) = min_part.trim().split_whitespace().nth(1) {
                    if let Ok(min) = min_string.parse::<u32>() {
                        current_node.min = min.to_string();
                    }
                }
            }

            if let Some(max_part) = max_part {
                if let Some(max_string) = max_part.trim().split_whitespace().nth(1) {
                    if let Ok(max) = max_string.parse::<u32>() {
                        current_node.max = max.to_string();
                    }
                }
            }

            if let Some(length_part) = length_part {
                if let Some(length_string) = length_part.trim().split_whitespace().nth(1) {
                    if let Ok(length) = length_string.parse::<u32>() {
                        current_node.length = length.to_string();
                    }
                }
            }
        }

        if info_complete {
            info_complete = false;
            // Writing line to csv
            writeln!(output_file, "{};{};{};{};{};{}",
                current_node.parent,
                current_node.fieldname,
                current_node.min,
                current_node.max,
                current_node.fieldtype,
                current_node.length
            )?;

            current_node.parent = String::new();
            current_node.fieldname = String::new();
            current_node.min = String::new();
            current_node.max = String::new();
            current_node.fieldname = String::new();
            current_node.length = String::new();
        } else {
            info_complete = true;
        }
    }

    // Stop the spinner thread
    should_stop.store(true, Ordering::Relaxed);
    spinner_handle.join().unwrap();
    progress.lock().unwrap().finish();

    Ok(())
}
